use flobot_lib::models::File;
use flobot_lib::models::FileInfos;
use regex::Regex;
use reqwest::blocking::Body;
use std::io::Cursor;

use super::models::*;
use flobot_lib::client::{
    Channel, Editor, Error, Getter, Notifier, Result, Sender, Uploader,
};
use flobot_lib::conf::Conf;
use flobot_lib::models as gm;
use log::debug;
use uuid::Uuid;

#[derive(Clone)]
pub struct Mattermost {
    pub cfg: Conf,
    me: Me,
    client: reqwest::blocking::Client,
}

impl Mattermost {
    pub fn new(cfg: Conf) -> Result<Self> {
        let client = reqwest::blocking::Client::new();
        let me: Me = client
            .get(&format!("{}/users/me", &cfg.api_url))
            .bearer_auth(&cfg.token)
            .send()?
            .json()?;
        println!("my user id: {}", me.id);
        Ok(Mattermost {
            cfg: cfg,
            me,
            client,
        })
    }

    fn url(&self, add: &str) -> String {
        let mut url = self.cfg.api_url.clone();
        url.push_str(add);
        url
    }
}

impl Channel for Mattermost {
    fn create_private(
        &self,
        team_id: &str,
        name: &str,
        users: &Vec<String>,
    ) -> Result<String> {
        let mut enc_buf = Uuid::encode_buffer();
        let uuid = Uuid::new_v4().to_simple().encode_lower(&mut enc_buf);
        let channel_name = format!("{}-{}", name, uuid).to_lowercase().clone();
        let mmchannel = CreateChannel {
            team_id: team_id,
            name: &channel_name,
            display_name: name,
            type_: "P",
        };

        let r: GenericID = self
            .client
            .post(&self.url("/channels"))
            .bearer_auth(&self.cfg.token)
            .json(&mmchannel)
            .send()?
            .json()?;

        for user_id in users.iter() {
            let uid = UserID {
                user_id: user_id.clone(),
            };
            self.client
                .post(&self.url(&format!("/channels/{}/members", r.id)))
                .bearer_auth(&self.cfg.token)
                .json(&uid)
                .send()?;
        }

        Ok(r.id)
    }

    fn archive(&self, channel_id: &str) -> Result<()> {
        self.client
            .delete(&self.url(&format!("/channels/{}", channel_id)))
            .bearer_auth(&self.cfg.token)
            .send()?;

        Ok(())
    }
}

impl Sender for Mattermost {
    fn post(&self, post: &gm::Post) -> Result<()> {
        return self.post_with_files(post, vec![]);
    }

    fn post_with_files(&self, post: &gm::Post, file_ids: Vec<String>) -> Result<()> {
        let mmpost = NewPost {
            channel_id: post.channel_id.clone(),
            create_at: 0,
            file_ids,
            message: &post.message,
            metadata: Metadata {},
            props: Props {},
            update_at: 0,
            user_id: self.me.id.clone(),
            parent_id: None,
            root_id: None,
        };
        let res = self
            .client
            .post(&self.url("/posts"))
            .bearer_auth(&self.cfg.token)
            .json(&mmpost)
            .send()?;

        debug!("Status: {}", res.status());
        Ok(())
    }

    fn reaction(&self, post: &gm::Post, reaction: &str) -> Result<()> {
        let reaction = Reaction {
            user_id: self.me.id.clone(),
            post_id: post.id.clone(),
            emoji_name: String::from(reaction),
        };
        let res = self
            .client
            .post(&self.url("/reactions"))
            .bearer_auth(&self.cfg.token)
            .json(&reaction)
            .send()?;

        debug!("Status: {}", res.status());
        Ok(())
    }

    fn reply(&self, post: &gm::Post, message: &str) -> Result<()> {
        let mmpost = NewPost {
            channel_id: post.channel_id.clone(),
            create_at: 0,
            file_ids: vec![],
            message: message,
            metadata: Metadata {},
            props: Props {},
            update_at: 0,
            user_id: self.me.id.clone(),
            parent_id: Some(post.id.clone()),
            root_id: Some(post.id.clone()),
        };
        let res = self
            .client
            .post(&self.url("/posts"))
            .bearer_auth(&self.cfg.token)
            .json(&mmpost)
            .send()?;

        debug!("Status: {}", res.status());
        Ok(())
    }
}

impl Editor for Mattermost {
    fn edit(&self, post: &gm::Post, message: &str) -> Result<()> {
        let edit = PostEdit {
            message: Some(message),
            file_ids: None,
        };

        let res = self
            .client
            .put(&self.url(&format!("/posts/{}/patch", post.id)))
            .bearer_auth(&self.cfg.token)
            .json(&edit)
            .send()?;

        debug!("Status: {}", res.status());
        Ok(())
    }
}

fn get_filename(name: String) -> String {
    let re = Regex::new(r"[^\w\s-]").unwrap();
    let name = re.replace_all(&name, "");
    let words: Vec<&str> = name.split_whitespace().take(3).collect();
    format!("{}.ogg", words.join("_"))
}

impl Uploader for Mattermost {
    fn upload(
        &self,
        post: &gm::Post,
        file: Cursor<Vec<u8>>,
        filename: Option<String>,
    ) -> Result<File> {
        let filename = get_filename(filename.unwrap_or(String::from("voices")));
        debug!("Upload file {} to: {}", filename, self.cfg.api_url);
        let channel_id = post.channel_id.clone();
        let binding = filename.to_string();
        let query = vec![("channel_id", &channel_id), ("filename", &binding)];
        let res = self
            .client
            .post(&self.url("/files"))
            .body(Body::new(file))
            .query(&query)
            .bearer_auth(&self.cfg.token)
            .send()?;

        debug!("Status: {}", res.status());
        if !res.status().is_success() {
            return Err(Error::Other(res.text()?));
        }

        match res.json::<FileInfos>() {
            Ok(parsed_file) => {
                if parsed_file.file_infos.len() == 0 {
                    return Err(Error::Other("Empty file_infos !".to_string()));
                }
                return Ok(parsed_file.file_infos[0].clone());
            }
            Err(err) => {
                return Err(Error::Other(format!("Cannot parse response. {}", err)));
            }
        }
    }
}

impl Notifier for Mattermost {
    fn startup(&self, message: &str) -> Result<()> {
        let datetime = chrono::offset::Local::now();
        let post = gm::Post::with_message(&format!(
            "# Startup {:?} (local time)\n## Build Hash\n * `{}`\n{}",
            datetime,
            flobot_lib::BUILD_GIT_HASH,
            message
        ))
        .nchannel(&self.cfg.debug_channel);
        self.post(&post)
    }

    fn required_action(&self, message: &str) -> Result<()> {
        let post = gm::Post::with_message(message).nchannel(&self.cfg.debug_channel);
        self.post(&post)
    }

    fn debug(&self, message: &str) -> Result<()> {
        let post = gm::Post::with_message(message).nchannel(&self.cfg.debug_channel);
        self.post(&post)
    }

    fn error(&self, message: &str) -> Result<()> {
        self.debug(message)
    }
}

impl Getter for Mattermost {
    fn my_user_id(&self) -> &str {
        &self.me.id
    }

    fn users_by_ids(&self, ids: Vec<&str>) -> Result<Vec<gm::User>> {
        let r = self
            .client
            .post(self.url("/users/ids"))
            .bearer_auth(&self.cfg.token)
            .json(&ids)
            .send()?;

        let users: Vec<User> = r.json()?;

        let mut fusers: Vec<gm::User> = vec![];
        for u in users.iter() {
            fusers.push((*u).clone().into());
        }

        Ok(fusers)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockito::Matcher;
    use std::io::Cursor;

    fn get_mattermost_client(server_url: String) -> Mattermost {
        let _ = env_logger::builder().is_test(true).try_init();
        let me = Me {
            id: "my_id".to_string(),
            username: "my_username".to_string(),
            email: "my@email.org".to_string(),
            nickname: "my_nickname".to_string(),
            first_name: "my_firstname".to_string(),
            last_name: "my_lastname".to_string(),
            is_bot: true,
        };
        let cfg = Conf {
            api_url: server_url,
            token: "FAKE_TOKEN".to_string(),
            ws_url: "".to_string(),
            debug_channel: "debug".to_string(),
            db_url: "".to_string(),
        };
        Mattermost {
            cfg,
            me: me.clone(),
            client: reqwest::blocking::Client::new(),
        }
    }

    #[test]
    fn test_get_filename() {
        assert_eq!(get_filename("toto".to_string()), "toto.ogg");
        assert_eq!(
            get_filename("Message à caractère informatif".to_string()),
            "Message_à_caractère.ogg"
        );
        assert_eq!(
            get_filename("C'était très intéressant ! Non ?".to_string()),
            "Cétait_très_intéressant.ogg"
        );
    }

    #[test]
    fn test_upload_file() {
        let mut server = mockito::Server::new();
        let client = get_mattermost_client(server.url());
        let message = String::from("Here is a file");
        let filename = get_filename("voices".to_string());
        let post = gm::Post {
            id: "post_id".to_string(),
            channel_id: "channel_id".to_string(),
            message,
            user_id: client.me.id.clone(),
            root_id: "".to_string(),
            parent_id: "".to_string(),
            team_id: "".to_string(),
        };
        let file_content: Vec<u8> = vec![
            0x4F, 0x67, 0x67, 0x53, // OggS (Header)
            0x00, 0x00, 0x00, 0x01, // Version
            0x00, 0x00, 0x00, 0x00, // Header Type
            0x00, 0x00, 0x00, 0x00, // Granule Position
            0x00, 0x00, 0x00, 0x00, // Bitstream Serial Number
            0x00, 0x00, 0x00, 0x00, // Sequence Number
            0x00, 0x00, 0x00, 0x00, // Timestamp
            0xFF, 0xFF, 0xFF, 0xFF, // Packet Length
            0x01, 0x00, 0x00, 0x00, // Page Sequence Number
            0x00, 0x00, 0x00, 0x00, // CRC
            0x00, 0x00, 0x00, 0x00, // Data
        ];
        let file = Cursor::new(file_content);

        let response = format!(
            r#"{{"file_infos": [
    {{
      "id": "file_id",
      "user_id": "{}",
      "channel_id": "channel_id",
      "create_at": 0,
      "update_at": 0,
      "delete_at": 0,
      "name": "{}.ogg",
      "extension": "ogg",
      "size": 1000,
      "mime_type": "audio/ogg",
      "mini_preview": null,
      "remote_id": "",
      "archived": false
    }}
  ],
  "client_ids": []
}}"#,
            client.me.id.clone(),
            filename
        );
        let mock_server = server
            .mock("POST", "/files")
            .match_header("Authorization", Matcher::Regex(r"^Bearer .+".to_string()))
            .match_query(Matcher::UrlEncoded(
                "channel_id".into(),
                post.channel_id.clone(),
            ))
            .match_query(Matcher::UrlEncoded("filename".into(), filename.into()))
            .with_status(201)
            .with_body(response)
            .create();

        let result = client.upload(&post, file, None);

        match result {
            Ok(_) => println!("Upload successful."),
            Err(e) => {
                panic!("Test failed due to error: {:?}", e);
            }
        }
        mock_server.assert();
        assert!(result.is_ok());
        mock_server.assert();
    }

    #[test]
    fn test_post_message() {
        let mut server = mockito::Server::new();
        let client = get_mattermost_client(server.url());
        let post = gm::Post {
            id: "post_id".to_string(),
            channel_id: "channel_id".to_string(),
            message: "Here is a massage".to_string(),
            user_id: client.me.id.clone(),
            root_id: "".to_string(),
            parent_id: "".to_string(),
            team_id: "".to_string(),
        };

        let response = format!(
            r#"{{
    "id": "post_id",
    "create_at": 0,
    "update_at": 0,
    "delete_at": 0,
    "edit_at": 0,
    "user_id": "{}",
    "channel_id": "channel_id",
    "root_id": "root_id",
    "original_id": "original_id",
    "message": "Here is a message",
    "type": "string",
    "props": {{}},
    "hashtag": "string",
    "file_ids": [],
    "pending_post_id": "string",
    "metadata":
    {{
        "embeds": [],
        "emojis": [],
        "files": [],
        "images": {{}},
        "reactions": [],
        "priority": {{}},
        "acknowledgements": []
    }}
}}"#,
            client.me.id.clone()
        );
        let mock_server = server
            .mock("POST", "/posts")
            .match_header("Authorization", Matcher::Regex(r"^Bearer .+".to_string()))
            .match_body(Matcher::Regex(post.channel_id.clone())) // Checking that channel ID is included
            .with_status(201)
            .with_body(response)
            .create();

        let result = client.post(&post);

        match result {
            Ok(_) => println!("Post successful."),
            Err(e) => {
                panic!("Test failed due to error: {:?}", e);
            }
        }
        mock_server.assert();
        assert!(result.is_ok());
        mock_server.assert();
    }
}
