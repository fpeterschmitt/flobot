#!/bin/bash

echo "Flobot upgrade."

if [ -f flobot.upgrade ]; then
	echo "Upgrade found, moving files"
	md5sum flobot.upgrade flobot
	mv flobot.upgrade flobot
	chmod +x flobot
else
	echo "No upgrade found."
fi
