use crate::cipher_helpers::CountryCodes;
use reqwest::blocking::Client;
use std::error::Error;
use std::io::Cursor;
use std::io::Write;
use tts_urls::voicerss::Codec;
use tts_urls::voicerss::VoiceRSSOptions;

pub struct TTS {
    key: String,
    codec: Codec,
}

pub type VoiceMessage = Cursor<Vec<u8>>;

impl TTS {
    // Generate request url for text synthesis
    fn get_synthesize_url(&self, text: &str, country_code: &CountryCodes) -> String {
        let (language, voice) = country_code.get_synthetize_parameters();
        VoiceRSSOptions::new()
            .language(language)
            .audio_format("32khz_16bit_stereo")
            .voice(voice)
            .codec(self.codec)
            .url(&self.key, text)
    }

    // Synthesize a message in a specific language
    pub fn synthesize(
        &self,
        text: &str,
        country_code: &CountryCodes,
    ) -> std::result::Result<VoiceMessage, Box<dyn Error>> {
        let url = self.get_synthesize_url(text, country_code);
        let response = Client::new().get(url).send()?.bytes()?;
        let mut cursor = Cursor::new(Vec::new());
        cursor.write_all(&response)?;
        cursor.set_position(0);
        Ok(cursor)
    }

    pub fn new(key: &str) -> TTS {
        TTS {
            key: key.to_string(),
            codec: Codec::OGG,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_synthesize_url() {
        let tts = TTS::new("KEY");
        let text = "Bonjour";
        assert_eq!(
            tts.get_synthesize_url(text, &CountryCodes::FR),
            format!(
                "http://api.voicerss.org/?key={}&hl=fr-fr&v=Bette&c=ogg&f=32khz_16bit_stereo&src={}",
                tts.key, text
            )
        )
    }
}
