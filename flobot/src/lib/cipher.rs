use crate::text_to_speech::TTS;
use flobot_lib::client;
use flobot_lib::handler::Handler as BotHandler;
use flobot_lib::models::Post;
use log::info;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fmt;

use crate::cipher_helpers::{
    compute_shift, generate_alphabet, get_country_code, get_simple_shift, normalize,
    orally_heard, orally_said, substitute, CountryCodes,
};

#[derive(Debug)]
pub enum Error {
    Client(String),
    Input(String),
    Other(String),
}

impl From<Error> for flobot_lib::handler::Error {
    fn from(e: Error) -> Self {
        match e {
            Error::Client(s) => flobot_lib::handler::Error::Timeout(s),
            Error::Input(s) => flobot_lib::handler::Error::Other(s),
            Error::Other(s) => flobot_lib::handler::Error::Other(s),
        }
    }
}

pub type Result = std::result::Result<String, Error>;

pub trait Cipher {
    fn cipher(&self, text: &str) -> Result;
    fn decipher(&self, text: &str) -> Result;
}

/// Init correct struct based on typed algorithm
trait FromAlgorithm<S> {
    fn from_algorithm(algorithm: &str) -> Option<S>;
}

/// Simple substitution vector
struct Substitution {
    key: Vec<i8>,
}

impl fmt::Display for Substitution {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Substitution()")
    }
}

impl FromAlgorithm<Substitution> for Substitution {
    fn from_algorithm(algorithm: &str) -> Option<Substitution> {
        let re = Regex::new(r"(shift|rot|vigenere)?\+?[a-zA-Z]*").unwrap();
        match re.captures(algorithm) {
            Some(capture) => match capture.get(1).map(|m| m.as_str()) {
                Some("") | Some("shift") | Some("rot") | None => {
                    if let Ok(shift) = get_simple_shift(algorithm) {
                        return Some(Substitution { key: vec![shift] });
                    }
                    return None;
                }
                Some("vigenere") => {
                    let key_vector: Vec<i8> = algorithm
                        .trim_start_matches("vigenere+")
                        .chars()
                        .filter(|c| c.is_alphanumeric())
                        .map(|c| compute_shift(c))
                        .collect();
                    return Some(Substitution { key: key_vector });
                }
                _ => return None,
            },
            None => return None,
        };
    }
}

impl Cipher for Substitution {
    fn cipher(&self, text: &str) -> Result {
        Ok(substitute(text, self.key.clone()))
    }

    fn decipher(&self, text: &str) -> Result {
        let key_vector: Vec<i8> = self.key.iter().map(|c| -c).collect();
        Ok(substitute(text, key_vector))
    }
}

/// All available algorithms
enum Algorithms {
    Radio(CountryCodes),
    Shift(Substitution),
    Vigenere(Substitution),
    Delastelle(Nomial),
}

impl fmt::Display for Algorithms {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Algorithms::Radio(_) => write!(f, "Radio()"),
            Algorithms::Shift(_) => write!(f, "Shift()"),
            Algorithms::Vigenere(_) => write!(f, "Vigenere()"),
            Algorithms::Delastelle(_) => write!(f, "Delastelle()"),
        }
    }
}

type PolybiusRectangle = HashMap<i8, Vec<char>>;
const SWITCH_KEY: char = '-';

/// Check and find a char in Polybius rectangle
trait ContainsChar {
    fn contains(&self, c: &char) -> bool;
    fn get_coordinates(&self, c: &char) -> String;
}

impl ContainsChar for PolybiusRectangle {
    fn contains(&self, c: &char) -> bool {
        for vec in self.values() {
            if vec.contains(&c) {
                return true;
            }
        }
        false
    }

    fn get_coordinates(&self, c: &char) -> String {
        if (*c == ' ' || *c == '.' || c.is_alphabetic()) && *c != SWITCH_KEY {
            let uppercase_c = c.to_uppercase().next().unwrap();
            for (key, val) in self.iter() {
                match val.iter().position(|&d| d == uppercase_c) {
                    Some(index) => {
                        if *key == -1 {
                            return index.to_string();
                        } else {
                            return key.to_string() + &index.to_string();
                        }
                    }
                    None => continue,
                }
            }
        }
        String::new()
    }
}

/// Delastelle's cipher parameters
struct Nomial {
    binome: String,
    square: PolybiusRectangle,
}

impl fmt::Display for Nomial {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Delastelle({})", self.binome)
    }
}

/// Specific trait for Delastelle based cipher
trait NomialTrait {
    fn new(monome: &str, binome: &str) -> std::result::Result<Nomial, Error>;
    /// Generate the Polybius rectangle
    fn get_polybius_square(monome: &str, binome: HashSet<i8>) -> PolybiusRectangle;
}

impl Cipher for Nomial {
    fn cipher(&self, text: &str) -> Result {
        let mut result = String::new();
        for c in normalize(text).chars() {
            result.push_str(&self.square.get_coordinates(&c));
        }
        Ok(result)
    }

    fn decipher(&self, text: &str) -> Result {
        let mut result = String::new();
        let mut mode: i8 = -1;
        for c in text.chars() {
            let pivot: i8;
            if let Some(digit) = c.to_digit(10) {
                pivot = digit as i8;
            } else {
                continue;
            }
            if mode == -1 && self.binome.contains(c) {
                mode = pivot;
                continue;
            }
            if let Some(d) = self.square[&mode].get(pivot as usize) {
                result.push(*d);
            }
            mode = -1
        }
        Ok(result)
    }
}

impl FromAlgorithm<Nomial> for Nomial {
    fn from_algorithm(algorithm: &str) -> Option<Nomial> {
        let re = Regex::new(r"delastelle\+([a-zA-Z]+)(\d{2})").unwrap();
        match re.captures(algorithm) {
            Some(capture) => {
                match Nomial::new(
                    capture.get(1).unwrap().as_str(),
                    capture.get(2).unwrap().as_str(),
                ) {
                    Ok(nomial) => Some(nomial),
                    Err(e) => {
                        info!("{:?}", e);
                        return None;
                    }
                }
            }
            None => return None,
        }
    }
}

impl NomialTrait for Nomial {
    fn new(monome: &str, binome: &str) -> std::result::Result<Nomial, Error> {
        let binome_ = HashSet::from([
            binome.chars().nth(0).unwrap().to_digit(10).unwrap() as i8,
            binome.chars().nth(1).unwrap().to_digit(10).unwrap() as i8,
        ]);
        if binome_.len() != 2 {
            return Err(Error::Input(
                "Binomial numbers must be different.".to_string(),
            ));
        }
        Ok(Nomial {
            binome: binome.to_string(),
            square: Nomial::get_polybius_square(monome, binome_),
        })
    }

    fn get_polybius_square(monome: &str, binome: HashSet<i8>) -> PolybiusRectangle {
        let mut rectangle = HashMap::new();
        let mut binome_: Vec<i8> = binome.iter().map(|&x| x as i8).collect();
        binome_.sort();
        let alphabet = generate_alphabet();

        let mut line = vec![' '; 10];
        line[binome_[0] as usize] = SWITCH_KEY;
        line[binome_[1] as usize] = SWITCH_KEY;
        let mut index = 0;
        for c in monome
            .to_uppercase()
            .chars()
            .chain(alphabet.iter().cloned())
        {
            if line.contains(&c) || rectangle.contains(&c) {
                continue;
            }
            while line[index] == SWITCH_KEY {
                index += 1;
            }
            line[index] = c;
            index += 1;
            if index >= 10 {
                // Flush line
                for &i in &[-1, binome_[0], binome_[1]] {
                    if rectangle.contains_key(&i) {
                        continue;
                    }
                    rectangle.insert(i, line.clone());
                    line = vec![' '; 10]; // clear
                    index = 0;
                    break;
                }
            }
        }
        if !line.is_empty() {
            rectangle.insert(binome_[1], line.clone());
        }
        rectangle
    }
}

/// Init correct Algorithm based on detected cipher
fn create_algorithm(algorithm: &str) -> Option<Algorithms> {
    match algorithm {
        alg if alg.is_empty() || alg.starts_with("shift") || alg.starts_with("rot") => {
            if let Some(shift) = Substitution::from_algorithm(algorithm) {
                return Some(Algorithms::Shift(shift));
            }
        }
        alg if alg.starts_with("vigenere") => {
            if let Some(vector) = Substitution::from_algorithm(algorithm) {
                return Some(Algorithms::Vigenere(vector));
            }
        }
        alg if alg.starts_with("delastelle") => {
            if let Some(nomial) = Nomial::from_algorithm(algorithm) {
                return Some(Algorithms::Delastelle(nomial));
            }
        }
        alg if alg.starts_with("radio") => {
            let re = Regex::new(r"radio\+?([a-zA-Z]+)").unwrap();
            if let Some(code) = re
                .captures(algorithm.to_lowercase().as_str())?
                .get(1)
                .map(|m| m.as_str())
                .and_then(|arg0: &str| get_country_code(Some(arg0)))
            {
                return Some(Algorithms::Radio(code));
            }
        }
        _ => return None,
    }
    None
}

impl Cipher for Algorithms {
    fn cipher(&self, text: &str) -> std::result::Result<String, Error> {
        match self {
            Algorithms::Radio(value) => {
                info!("Texting using Radio code");
                Ok(orally_said(value, text, None))
            }
            Algorithms::Shift(value) => {
                info!("Ciphering using Shift");
                Ok(value.cipher(text)?)
            }
            Algorithms::Vigenere(value) => {
                info!("Ciphering using Vigenere");
                Ok(value.cipher(text)?)
            }
            Algorithms::Delastelle(value) => {
                info!("Ciphering using Delastelle++");
                Ok(value.cipher(text)?)
            }
        }
    }

    fn decipher(&self, text: &str) -> std::result::Result<String, Error> {
        match self {
            Algorithms::Radio(value) => {
                info!("Texting using Radio code");
                Ok(orally_heard(value, text))
            }
            Algorithms::Shift(value) => {
                info!("Deciphering using Shift");
                Ok(value.decipher(text)?)
            }
            Algorithms::Vigenere(value) => {
                info!("Deciphering using Vigenere");
                Ok(value.decipher(text)?)
            }
            Algorithms::Delastelle(value) => {
                info!("Deciphering using Delastelle++");
                Ok(value.decipher(text)?)
            }
        }
    }
}

pub struct Handler<D>
where
    D: flobot_lib::client::Editor,
    D: flobot_lib::client::Uploader,
    D: flobot_lib::client::Sender,
{
    client: D,
    tts_client: std::option::Option<TTS>,
}

pub enum CipherMode {
    Cipher,
    Decipher,
}

pub enum RadioMode {
    Spell,
    Say,
}

impl<D> Handler<D>
where
    D: client::Sender + client::Editor + client::Uploader,
{
    pub fn new(client: D, tts_client: Option<TTS>) -> Self {
        Handler { client, tts_client }
    }

    fn parse_command<'a>(
        &self,
        input: &'a str,
        regex: &Regex,
    ) -> std::result::Result<(Option<&'a str>, &'a str), String> {
        regex
            .captures(input)
            .ok_or_else(|| format!("Unmatched pattern: {}", input))
            .and_then(|caps| {
                caps.get(2)
                    .ok_or_else(|| {
                        format!("Missing second capture group in input: {}", input)
                    })
                    .map(|m2| (caps.get(1).map(|m| m.as_str()), m2.as_str()))
            })
    }

    fn process_cipher(
        &self,
        post: &Post,
        input: &str,
        mode: CipherMode,
    ) -> flobot_lib::handler::Result {
        let re = Regex::new(r"^!(?:cipher|decipher)\s?!?([\w\+]+)?\s+(.*)$").unwrap();
        let (algorithm, message) = match self.parse_command(input, &re) {
            Ok(result) => result,
            Err(e) => {
                let _ = self.reply_error(post, e.clone());
                return Err(Error::Input(e).into());
            }
        };

        let algorithm = match create_algorithm(algorithm.unwrap_or("")) {
            Some(alg) => alg,
            None => {
                self.reply_error(
                    post,
                    format!("Unparsable algorithm: {:?}", algorithm.unwrap()),
                );
                return Ok(());
            }
        };

        let processed_message = match match mode {
            CipherMode::Cipher => algorithm.cipher(message),
            CipherMode::Decipher => algorithm.decipher(message),
        } {
            Ok(result) => result,
            Err(error) => {
                self.reply_error(post, format!("Cannot process: {:?}", error));
                return Ok(());
            }
        };

        self.client.reaction(post, "sleuth_or_spy")?;
        Ok(self.client.edit(post, &processed_message)?)
    }

    fn process_radio(
        &self,
        post: &Post,
        input: &str,
        mode: RadioMode,
    ) -> flobot_lib::handler::Result {
        let re = Regex::new(r"^!(?:radio|say)\+([\w]+)\s+(.*)$").unwrap();
        let (algorithm, message) = match self.parse_command(input, &re) {
            Ok(result) => result,
            Err(e) => {
                let _ = self.reply_error(post, e.clone());
                return Err(Error::Input(e).into());
            }
        };

        let code = match algorithm.and_then(|arg0: &str| get_country_code(Some(arg0))) {
            Some(code) => code,
            None => {
                self.reply_error(
                    post,
                    format!("Unsupported country code: {:?}", algorithm.unwrap()),
                );
                return Ok(());
            }
        };

        let sentence = match mode {
            RadioMode::Spell => orally_said(&code, message, Some(true)),
            RadioMode::Say => message.to_string(),
        };
        let voice_message = match &self.tts_client {
            Some(tts) => match tts.synthesize(&sentence, &code) {
                Ok(message) => message,
                Err(e) => {
                    self.reply_error(post, e.to_string());
                    return Ok(());
                }
            },
            None => {
                self.reply_error(
                    post,
                    "Text to speech service is unavailable".to_string(),
                );
                return Ok(());
            }
        };

        let upload =
            match self
                .client
                .upload(post, voice_message, Some(message.to_string()))
            {
                Ok(upload_result) => upload_result,
                Err(e) => {
                    self.reply_error(post, e.to_string());
                    return Ok(());
                }
            };

        Ok(self.client.post_with_files(
            &post.nmessage(":speaking_head_in_silhouette:"),
            vec![upload.id],
        )?)
    }
}

impl<D> BotHandler for Handler<D>
where
    D: client::Sender + client::Editor + client::Uploader,
{
    type Data = Post;

    fn name(&self) -> String {
        "cipher".into()
    }
    fn help(&self) -> Option<String> {
        Some(
            "`!cipher` *text* : :clipboard: cipher that with caesar algorithm, now!
`!cipher` !**algorithm** *text* : :clipboard: cipher using a specific algorithm
`!decipher` !**algorithm** *text* : :clipboard: decipher a text with the right algorithm
`!cipher-algorithms` : :clipboard: list all available algorithms
`!radio+LANGUAGE` (`be`, `fr`, `eo`, `icao`, or `int`) : :clipboard: convert a message to radio spelling, through an audio message
`!say+LANGUAGE` (`be`, `fr`, `eo`, `icao`, or `int`) : :clipboard: convert a message to an audio message

*Please note that all cipherings will lose all diacritical symbols but will leave most punctation symbols as is*

Examples:
* `!cipher !rot13 Secret message` will shift all letters 13 letters ahead, producing: \"*Frperg zrffntr*\"
* `!decipher !rot13 Frperg zrffntr` gives your message back: \"*Secret Message\"

Bad examples:
* `!cipher @MESSAGE with this key` is not a proper command so you shall not use it
* `!decipher Frperg zrffntr` will work but not as expected because it don't know how to decode it. Make a guest !
* `five is right out !` is not even a command, so snuff it

« Dona eis requiem » *Brother Maynard*
".to_string(),
        )
    }

    fn handle(&self, post: &Post) -> flobot_lib::handler::Result {
        let input = &post.message;

        if input.starts_with("!cipher-algorithms") {
            let help_message = "* `!shift0` to `26` (equivalent with: `!rot0` to `26`): :mag_right: Circular permutation of letters (aka *'[Caesar code](https://en.wikipedia.org/wiki/Caesar_cipher)'*) ; the number is the permutation size (yes, 0 and 26 are almost doing nothing :)). Easy to break with brut force.
ex: `!cipher !shift3 message` gives \"*phvvdjh*\"
* `!vigenere+KEYWITHOUTSPACES`: :mag_right: [Vigenère cipher](https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher) do letter permutations according to a certain key (non-alphanumerical characters are ignored)
ex: `!cipher !vigenere+SECRETKEY message` gives \"*eiujezo*\"
* `!delastelle+KEYXY` (with a key AND two distinct digits *XY*): :mag_right: Its an improved version of one of Delastelle's cipher (called « [monomial-binomial](https://fr.wikipedia.org/wiki/Chiffre_m%C3%B4nome_bin%C3%B4me) » cipher) based on a generated [Polybius square](https://en.wikipedia.org/wiki/Polybius_square) (keeps only full dots). Hard to break manually, even with statistical analysis.
ex: `!cipher !delastelle+SECRETKEY12 message` gives \"*183009133*\"
* `!radio+LANGUAGE` (`be`, `fr`, `eo`, `icao`, or `int`): :mag_right: Not really a cipher but it spells the message with a [specific spelling alphabet](https://en.wikipedia.org/wiki/Spelling_alphabet) (used for instance in radio communications) (keeps only full dots and comas)
ex: `!cipher !radio+icao message` gives \"*MikeEchoSierraSierraAlfaGolfEcho*\"";
            return Ok(self.client.reply(post, help_message)?);
        } else if input.starts_with("!cipher") {
            self.process_cipher(post, input, CipherMode::Cipher)
        } else if input.starts_with("!decipher") {
            self.process_cipher(post, input, CipherMode::Decipher)
        } else if input.starts_with("!radio") {
            self.process_radio(post, input, RadioMode::Spell)
        } else if input.starts_with("!say") {
            self.process_radio(post, input, RadioMode::Say)
        } else {
            Ok(())
        }
    }
}

#[cfg(test)]
mod cipher {
    use super::*;

    #[test]
    fn test_polybius_square_contains() -> std::result::Result<(), Error> {
        let square: PolybiusRectangle = HashMap::from([
            (
                -1,
                vec![
                    'R', 'E', SWITCH_KEY, 'T', 'O', 'A', 'H', SWITCH_KEY, 'Y', 'B',
                ],
            ),
            (2, vec!['C', 'D', 'F', 'G', 'I', 'J', 'K', 'L', 'M', 'N']),
        ]);
        assert!(square.contains(&'A'));
        assert!(square.contains(&'R'));
        assert!(square.contains(&'J'));
        assert!(!square.contains(&'Z'));
        assert!(!square.contains(&' '));
        assert!(!square.contains(&'!'));
        Ok(())
    }

    #[test]
    fn test_polybius_square_get_coordinates() -> std::result::Result<(), Error> {
        let square: PolybiusRectangle = HashMap::from([
            (
                -1,
                vec![
                    'R', 'E', SWITCH_KEY, 'T', 'O', 'A', 'H', SWITCH_KEY, 'Y', 'B',
                ],
            ),
            (2, vec!['C', 'D', 'F', 'G', 'I', 'J', 'K', 'L', 'M', 'N']),
            (7, vec!['P', 'Q', 'S', 'U', 'V', 'W', 'X', 'Z', '.', ' ']),
        ]);
        assert_eq!(square.get_coordinates(&'R'), "0");
        assert_eq!(square.get_coordinates(&'r'), "0");
        assert_eq!(square.get_coordinates(&'Y'), "8");
        assert_eq!(square.get_coordinates(&'y'), "8");
        assert_eq!(square.get_coordinates(&'C'), "20");
        assert_eq!(square.get_coordinates(&'c'), "20");
        assert_eq!(square.get_coordinates(&'I'), "24");
        assert_eq!(square.get_coordinates(&'i'), "24");
        assert_eq!(square.get_coordinates(&'Z'), "77");
        assert_eq!(square.get_coordinates(&'z'), "77");
        assert_eq!(square.get_coordinates(&'1'), "");
        assert_eq!(square.get_coordinates(&'9'), "");
        assert_eq!(square.get_coordinates(&'.'), "78");
        assert_eq!(square.get_coordinates(&' '), "79");
        assert_eq!(square.get_coordinates(&'!'), "");
        assert_eq!(square.get_coordinates(&SWITCH_KEY), "");
        Ok(())
    }

    #[test]
    fn test_nomial_get_polybius_square() -> std::result::Result<(), Error> {
        let set: HashSet<i8> = HashSet::from([2 as i8, 7 as i8]);
        let result: PolybiusRectangle = HashMap::from([
            (
                -1,
                vec![
                    'R', 'E', SWITCH_KEY, 'T', 'O', 'A', 'H', SWITCH_KEY, 'Y', 'B',
                ],
            ),
            (2, vec!['C', 'D', 'F', 'G', 'I', 'J', 'K', 'L', 'M', 'N']),
            (7, vec!['P', 'Q', 'S', 'U', 'V', 'W', 'X', 'Z', '.', ' ']),
        ]);
        assert_eq!(
            Nomial::get_polybius_square("RETROAHOY", set.clone()),
            result
        );
        assert_eq!(Nomial::get_polybius_square("ReTroAhOY", set), result);

        let set: HashSet<i8> = HashSet::from([2 as i8, 1 as i8]);
        let result: PolybiusRectangle = HashMap::from([
            (-1, vec!['B', '-', '-', 'O', 'N', 'J', 'U', 'R', 'A', 'C']),
            (1, vec!['D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'P']),
            (2, vec!['Q', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z', '.', ' ']),
        ]);
        assert_eq!(Nomial::get_polybius_square("BONJOUR", set), result);
        Ok(())
    }

    #[test]
    fn test_nomial_cipher() -> std::result::Result<(), Error> {
        let result = Nomial::new("RETROAHOY", "77");
        assert!(result.is_err());

        let nomial = Nomial::new("RETROAHOY", "27").unwrap();
        assert_eq!(nomial.cipher("")?, "");
        assert_eq!(nomial.cipher("TRON")?, "30429");
        assert_eq!(nomial.cipher("É-TRON")?, "130429");
        assert_eq!(nomial.cipher("À'TRON-TÏon")?, "530429324429");
        assert_eq!(
            nomial.cipher("Modern software dev is, Mostly, overhead !")?,
            "284211029797242237550179211747924727928472327879474106152179"
        );
        Ok(())
    }

    #[test]
    fn test_nomial_decipher() -> std::result::Result<(), Error> {
        let nomial = Nomial::new("RETROAHOY", "27").unwrap();
        assert_eq!(nomial.decipher("")?, "");
        assert_eq!(nomial.decipher("30429")?, "TRON");
        assert_eq!(nomial.decipher("130429")?, "ETRON");
        assert_eq!(nomial.decipher("530429324429")?, "ATRONTION");
        assert_eq!(
            nomial.decipher(
                "284211029797242237550179211747924727928472327879474106152179"
            )?,
            "MODERN SOFTWARE DEV IS MOSTLY OVERHEAD "
        );
        Ok(())
    }

    #[test]
    fn test_cipher_radio() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("radio+FR").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!(
            "HenriEugèneLouisLouisOscarDécimale WilliamOscarRaoulLouisDésiré ",
            &r1
        );
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO, WORLD ", &r2);

        algo = create_algorithm("radio+ICAO").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!(
            "HotelEchoLimaLimaOscarDecimal WhiskeyOscarRomeoLimaDelta ",
            &r1
        );
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO, WORLD ", &r2);
        Ok(())
    }

    #[test]
    fn test_cipher_rot() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("shift3").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Khoor, Zruog !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Khoor, Zruog !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("rot13").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Uryyb, Jbeyq !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);
        Ok(())
    }

    #[test]
    fn test_cipher_vigenere() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("vigenere+BONJOUR").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Isyuc, Qfszq !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+BON...").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Isymc, Jpfye !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+LES.SANGLOTS!LONGS-DES_VIOLONS.").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Siddo, Juczw !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+A").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("Hello, World !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);

        algo = create_algorithm("vigenere+D").unwrap();
        let r1 = algo.cipher("Héllo, World !")?; // aka Caesar
        assert_eq!("Khoor, Zruog !", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("Hello, World !", &r2);
        Ok(())
    }

    #[test]
    fn test_cipher_delastelle() -> std::result::Result<(), Error> {
        let mut algo = create_algorithm("delastelle+BONJOUR12").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("141117173292437171029", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO WORLD ", &r2);

        algo = create_algorithm("delastelle+BONJOUR21").unwrap();
        let r1 = algo.cipher("Héllo, World !")?;
        assert_eq!("141117173292437171029", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("HELLO WORLD ", &r2);

        let r1 = algo.cipher("Que j'aie une banque à chaque doigt ! Et un doigt dans chaque pays ! Que chaque pays soit à moi, Je sais quand même que chaque nuit...")?;
        assert_eq!("20611295815112964112908420611298299148206112910315132229291122296429103151322291084212991482061129198262129292061129914820611291982621292131522298291831529511292181521292068410291811181129206112991482061129461522282828", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("QUE JAIE UNE BANQUE A CHAQUE DOIGT  ET UN DOIGT DANS CHAQUE PAYS  QUE CHAQUE PAYS SOIT A MOI JE SAIS QUAND MEME QUE CHAQUE NUIT...", &r2);

        algo = create_algorithm("delastelle+SALUT26").unwrap();
        let r1 = algo.cipher("Bonjour! Tout le Monde")?;
        assert_eq!("72928252946269529456932069272928920", &r1);
        let r2 = algo.decipher(r1.as_str())?;
        assert_eq!("BONJOUR TOUT LE MONDE", &r2);
        Ok(())
    }
}
