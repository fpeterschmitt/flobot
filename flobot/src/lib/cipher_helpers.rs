use regex::Regex;
use std::collections::HashMap;
use std::fmt;

use crate::cipher::Error;
use log::debug;
use phf::phf_map;
use tts_urls::voicerss::Language;
use unicase::UniCase;
use unicode_normalization::UnicodeNormalization;

#[derive(Debug)]
pub enum CountryCodes {
    BE,
    EO,
    FR,
    ICAO,
    INT,
}
impl fmt::Display for CountryCodes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Country {:?}", self)
    }
}

impl CountryCodes {
    pub fn get_synthetize_parameters(&self) -> (Language, String) {
        match self {
            CountryCodes::BE => (Language::FrenchSwitzerland, "Theo".to_string()),
            CountryCodes::EO => (Language::Catalan, "Rut".to_string()),
            CountryCodes::FR => (Language::French, "Bette".to_string()),
            CountryCodes::ICAO => (Language::EnglishGreatBritain, "Alice".to_string()),
            CountryCodes::INT => (Language::EnglishUnitedStates, "Linda".to_string()),
        }
    }
}

pub type RadioMapping = phf::Map<char, &'static str>;
pub static RADIO_FR: RadioMapping = phf_map! {
    'A' => "Anatole",
    'B' => "Berther",
    'C' => "Célestine",
    'D' => "Désiré",
    'E' => "Eugène",
    'F' => "François",
    'G' => "Gastion",
    'H' => "Henri",
    'I' => "Irma",
    'J' => "Joseph",
    'K' => "Kléber",
    'L' => "Louis",
    'M' => "Marcel",
    'N' => "Nicolas",
    'O' => "Oscar",
    'P' => "Pierre",
    'Q' => "Quintal",
    'R' => "Raoul",
    'S' => "Suzanne",
    'T' => "Thérèse",
    'U' => "Ursule",
    'V' => "Victor",
    'W' => "William",
    'X' => "Xavier",
    'Y' => "Yvonne",
    'Z' => "Zoé",
    ' ' => " ",
    '.' => "STOP",
    '0' => "Zéro",
    '1' => "Un",
    '2' => "Deux",
    '3' => "Trois",
    '4' => "Quatre",
    '5' => "Cinq",
    '6' => "Six",
    '7' => "Sept",
    '8' => "Huit",
    '9' => "Neuf",
    ',' => "Décimale",
};
pub static RADIO_BE: RadioMapping = phf_map! {
    'A' => "Arthur",
    'B' => "Bruxelles",
    'C' => "César",
    'D' => "David",
    'E' => "Émilie",
    'F' => "Frédéric",
    'G' => "Gustave",
    'H' => "Henri",
    'I' => "Isidor",
    'J' => "Jeanne",
    'K' => "Kilogramme",
    'L' => "Léopold",
    'M' => "Marie",
    'N' => "Napoléon",
    'O' => "Oscar",
    'P' => "Piano",
    'Q' => "Quiévrain",
    'R' => "Robert",
    'S' => "Simon",
    'T' => "Téléphone",
    'U' => "Ursule",
    'V' => "Victor",
    'W' => "Waterloo",
    'X' => "Xantippe",
    'Y' => "Yvonne",
    'Z' => "Zéro",
    ' ' => " ",
    '.' => "STOP",
    '0' => "Zero",
    '1' => "Un",
    '2' => "Deux",
    '3' => "Trois",
    '4' => "Quatre",
    '5' => "Cinq",
    '6' => "Six",
    '7' => "Sept",
    '8' => "Ouît",
    '9' => "Neuf",
    ',' => "Décimale",
};
pub static RADIO_ICAO: RadioMapping = phf_map! {
    'A' => "Alfa",
    'B' => "Bravo",
    'C' => "Charlie",
    'D' => "Delta",
    'E' => "Echo",
    'F' => "Foxtrot",
    'G' => "Golf",
    'H' => "Hotel",
    'I' => "India",
    'J' => "Juliett",
    'K' => "Kilo",
    'L' => "Lima",
    'M' => "Mike",
    'N' => "November",
    'O' => "Oscar",
    'P' => "Papa",
    'Q' => "Québec",
    'R' => "Romeo",
    'S' => "Sierra",
    'T' => "Tango",
    'U' => "Uniform",
    'V' => "Victor",
    'W' => "Whiskey",
    'X' => "X-ray",
    'Y' => "Yankee",
    'Z' => "Zulu",
    ' ' => " ",
    '.' => "STOP",
    '0' => "Zero",
    '1' => "Wun",
    '2' => "Too",
    '3' => "Tree",
    '4' => "Fower",
    '5' => "Fife",
    '6' => "Six",
    '7' => "Seven",
    '8' => "Eight",
    '9' => "Niner",
    ',' => "Decimal",
};
pub static RADIO_INT: RadioMapping = phf_map! {
    'A' => "Amsterdam",
    'B' => "Baltimore",
    'C' => "Casablanca",
    'D' => "Danemark",
    'E' => "Edison",
    'F' => "Florida",
    'G' => "Gallipoli",
    'H' => "Havanna",
    'I' => "Italia",
    'J' => "Jerusalem",
    'K' => "Kilogram",
    'L' => "Liverpool",
    'M' => "Madagascar",
    'N' => "NewYork",
    'O' => "Oslo",
    'P' => "Paris",
    'Q' => "Québec",
    'R' => "Roma",
    'S' => "Santiago",
    'T' => "Tripoli",
    'U' => "Uppsala",
    'V' => "Valence",
    'W' => "Washington",
    'X' => "Xanthippe",
    'Y' => "Yokohama",
    'Z' => "Zurich",
    ' ' => " ",
    '.' => "STOP",
    '0' => "Zero",
    '1' => "One",
    '2' => "Two",
    '3' => "Three",
    '4' => "Four",
    '5' => "Five",
    '6' => "Six",
    '7' => "Seven",
    '8' => "Eight",
    '9' => "Nine",
    ',' => "Decimal",
};
pub static RADIO_EO: RadioMapping = phf_map! {
    'A' => "Alfa",
    'B' => "Bravo",
    'C' => "Carli",
    'D' => "Delta",
    'E' => "Eko",
    'F' => "Fokstrot",
    'G' => "Golf",
    'H' => "Hotel",
    'I' => "India",
    'J' => "Juliet",
    'K' => "Kilo",
    'L' => "Lima",
    'M' => "Majk",
    'N' => "November",
    'O' => "Oskar",
    'P' => "Papa",
    'Q' => "Kebek",
    'R' => "Romeo",
    'S' => "Siera",
    'T' => "Tango",
    'U' => "Unifrom",
    'V' => "Viktor",
    'W' => "Ŭiski", // Viski
    'X' => "Eksrej",
    'Y' => "Janki",
    'Z' => "Zulu",
    ' ' => " ",
    '.' => "HALTU",
    '0' => "Nulo",
    '1' => "Unu",
    '2' => "Du",
    '3' => "Tri",
    '4' => "Kvar",
    '5' => "Kvin",
    '6' => "Sis",
    '7' => "Sepen",
    '8' => "Ok",
    '9' => "Naŭ",
    ',' => "Decimalo",
};

/// Replace accentuated characters with regular ones
///
/// # Examples
///
/// ```
/// use crate::flobot::cipher_helpers::normalize;
///
/// assert_eq!(normalize("AzZa"), "AzZa");
/// assert_eq!(normalize("éÉ-àÀ-ïÏ"), "eE-aA-iI");
/// assert_eq!(normalize("02[]Ω²_"), "02[]_");
/// assert_eq!(normalize("Héllo, World !"), "Hello, World !");
/// ```
pub fn normalize(text: &str) -> String {
    let text = text.nfd().collect::<String>();
    let text: String = text.chars().filter(|c| c.is_ascii()).collect();
    text
}

/// Add (un)signed integers and stay in alphabetical space
///
/// # Examples
///
/// ```
/// use crate::flobot::cipher_helpers::add_i8_to_u8;
///
/// assert_eq!(add_i8_to_u8(12, 3), 15);
/// assert_eq!(add_i8_to_u8(12 + 26, 3), 15);
/// assert_eq!(add_i8_to_u8(12, 3 + 26), 15);
/// assert_eq!(add_i8_to_u8(12, -3), 9);
/// assert_eq!(add_i8_to_u8(12 + 26, -3), 9);
/// assert_eq!(add_i8_to_u8(12, -3 - 26), 9);
/// ```
pub fn add_i8_to_u8(value: u8, shift: i8) -> u8 {
    let result = (value as i16 + shift as i16).rem_euclid(26);
    result as u8
}

/// Apply a shift to a character
///
/// # Examples
///
/// ```
/// use crate::flobot::cipher_helpers::shift_char;
///
/// assert_eq!(shift_char('A', 0), 'A');
/// assert_eq!(shift_char('a', 1), 'b');
/// assert_eq!(shift_char('a', 26), 'a');
/// ```
pub fn shift_char(c: char, shift: i8) -> char {
    if c.is_ascii_uppercase() {
        (add_i8_to_u8(c as u8 - b'A', shift) % 26 + b'A') as char
    } else if c.is_ascii_lowercase() {
        (add_i8_to_u8(c as u8 - b'a', shift) % 26 + b'a') as char
    } else {
        c
    }
}

/// Compute a shift value from a character (= distance to A)
///
/// # Examples
///
/// ```
/// use crate::flobot::cipher_helpers::compute_shift;
///
/// assert_eq!(compute_shift('A'), 0);
/// assert_eq!(compute_shift('a'), 0);
/// assert_eq!(compute_shift('C'), 2);
/// assert_eq!(compute_shift('Z'), 25);
/// assert_eq!(compute_shift('d'), 3);
/// assert_eq!(compute_shift('!'), 0);
/// assert_eq!(compute_shift(' '), 0);
/// ```
pub fn compute_shift(c: char) -> i8 {
    if c.is_ascii_uppercase() {
        c as i8 - b'A' as i8
    } else if c.is_ascii_lowercase() {
        c as i8 - b'a' as i8
    } else {
        0
    }
}

/// Extract a shift value from the algorithm name
pub fn get_simple_shift(text: &str) -> std::result::Result<i8, Error> {
    if text == "" {
        return Ok(3);
    }
    let shift = match text
        .trim_start_matches("shift")
        .trim_start_matches("rot")
        .trim_start_matches("+")
        .parse()
    {
        Ok(num) => num,
        Err(_) => {
            return Err(Error::Other(
                format!("Unparsable shift algorithm {}", text).to_string(),
            ))
        }
    };
    Ok(shift)
}

/// Substitute each letter with another from a shift vector
pub fn substitute(text: &str, shifts: Vec<i8>) -> String {
    let shifts_len = shifts.len();
    let mut index = 0;
    let mut substitued = String::from("");
    for char_ in normalize(text).chars() {
        let mut next = char_.clone();
        if char_.is_alphanumeric() {
            next = shift_char(char_, shifts[index % shifts_len]);
            index += 1;
        }
        substitued.push(next);
    }
    substitued
}

/// Generate a standard alphabet
pub fn generate_alphabet() -> [char; 28] {
    let mut alphabet: [char; 28] = [' '; 28];
    for i in 0..26 {
        alphabet[i] = (b'A' + i as u8) as char;
    }
    alphabet[26] = '.';
    alphabet[27] = ' ';
    alphabet
}

/// Convert text to an actual country code
pub fn get_country_code(text: Option<&str>) -> Option<CountryCodes> {
    if text.is_none() {
        return Some(CountryCodes::FR);
    }
    match text.unwrap().to_uppercase().as_str() {
        "" | "FR" => {
            return Some(CountryCodes::FR);
        }
        "BE" => {
            return Some(CountryCodes::BE);
        }
        "EO" => {
            return Some(CountryCodes::EO);
        }
        "ICAO" => {
            return Some(CountryCodes::ICAO);
        }
        "INT" => {
            return Some(CountryCodes::INT);
        }
        _ => return None,
    }
}

/// Get he correct hashmap from country code
fn get_mapping(country: &CountryCodes) -> &'static RadioMapping {
    return match country {
        CountryCodes::BE => &RADIO_BE,
        CountryCodes::EO => &RADIO_EO,
        CountryCodes::FR => &RADIO_FR,
        CountryCodes::ICAO => &RADIO_ICAO,
        CountryCodes::INT => &RADIO_INT,
    };
}

/// Convert a text to spelling alphabet
pub fn orally_said(country: &CountryCodes, text: &str, spaced: Option<bool>) -> String {
    let mut phrase = String::from("");
    let mapping = get_mapping(&country);
    let spaced = spaced.unwrap_or(false);
    for char_ in normalize(text).to_uppercase().chars() {
        if let Some(word) = mapping.get(&char_) {
            phrase.push_str(*word);
            if spaced {
                phrase.push_str(" ");
            }
        } else {
            debug!("Character {} not found in mapping", char_);
        }
    }
    phrase
}

/// Convert spelling alphabet to text
pub fn orally_heard(country: &CountryCodes, text: &str) -> String {
    let mut phrase = String::from("");

    let reversed_map: HashMap<UniCase<&str>, char> = get_mapping(&country)
        .into_iter()
        .map(|(k, v)| (UniCase::new(*v), *k))
        .collect();
    let words_pattern = format!(
        r"(?i)({})",
        reversed_map
            .keys()
            .cloned()
            .map(|k| k.into_inner())
            .collect::<Vec<&str>>()
            .join("|")
    );
    let re = Regex::new(&words_pattern).unwrap();

    for capture in re.captures_iter(text) {
        let word = UniCase::new(capture.get(0).unwrap().as_str());
        if let Some(char_) = reversed_map.get(&word) {
            phrase.push_str(char_.to_string().as_str());
        } else {
            debug!("Word {} not found in mapping", word);
        }
    }
    phrase
}

#[cfg(test)]
mod cipher {
    use super::*;

    #[test]
    fn test_get_simple_shift() -> std::result::Result<(), Error> {
        assert_eq!(get_simple_shift("shift3")?, 3);
        assert_eq!(get_simple_shift("")?, 3);
        assert_eq!(get_simple_shift("rot9")?, 9);
        assert!(get_simple_shift("les_snorkies").is_err());
        assert!(get_simple_shift("13rot").is_err());
        assert!(get_simple_shift("rott").is_err()); // Rise of The Triad
        Ok(())
    }

    #[test]
    fn test_substitute() -> std::result::Result<(), Error> {
        assert_eq!(substitute("Héllo, World !", [0].to_vec()), "Hello, World !");
        assert_eq!(
            substitute("Héllo, World !", [0; 3].to_vec()),
            "Hello, World !"
        );
        assert_eq!(substitute("Héllo, World !", [3].to_vec()), "Khoor, Zruog !"); // aka Caesar
        assert_eq!(
            substitute("Khoor, Zruog !", [-3].to_vec()),
            "Hello, World !"
        ); // Complementary
        assert_eq!(
            substitute("Héllo, World !", [1, 2, 3, 4, 5].to_vec()),
            "Igopt, Xqupi !"
        );
        assert_eq!(
            substitute("Héllo, World !", [-25, -24, -23, -22, -21].to_vec()),
            "Igopt, Xqupi !"
        ); // Complementary
        assert_eq!(
            substitute("Héllo, World !", [15, 17, 14, 20, 19].to_vec()),
            "Wvzfh, Lfffw !"
        );
        Ok(())
    }

    #[test]
    fn test_shift_char() -> std::result::Result<(), Error> {
        assert_eq!(shift_char('A', 3), 'D');
        assert_eq!(shift_char('a', 3), 'd');
        assert_eq!(shift_char('Z', 3), 'C');
        assert_eq!(shift_char('z', 3), 'c');
        // Complementaries
        assert_eq!(shift_char('D', -3), 'A');
        assert_eq!(shift_char('d', -3), 'a');
        assert_eq!(shift_char('C', -3), 'Z');
        assert_eq!(shift_char('c', -3), 'z');

        assert_eq!(shift_char('A', 22), 'W');
        assert_eq!(shift_char('E', 22), 'A');
        assert_eq!(shift_char(normalize("É").chars().next().unwrap(), -4), 'A');
        assert_eq!(shift_char(normalize("è").chars().next().unwrap(), -4), 'a');
        // Null op
        assert_eq!(shift_char('Q', 26), 'Q');
        assert_eq!(shift_char('Q', 0), 'Q');
        Ok(())
    }

    #[test]
    fn test_get_country_code() -> std::result::Result<(), Error> {
        assert!(get_country_code(Some(" ")).is_none());
        assert!(get_country_code(Some("jp")).is_none());
        assert!(get_country_code(Some("screugneugneu")).is_none());
        matches!(get_country_code(Some("")).unwrap(), CountryCodes::FR);
        matches!(get_country_code(Some("fr")).unwrap(), CountryCodes::FR);
        matches!(get_country_code(Some("FR")).unwrap(), CountryCodes::FR);
        matches!(get_country_code(Some("eo")).unwrap(), CountryCodes::EO);
        matches!(get_country_code(Some("icao")).unwrap(), CountryCodes::ICAO);
        Ok(())
    }

    #[test]
    fn test_get_synthetize_parameters() -> std::result::Result<(), Error> {
        let code = CountryCodes::FR;
        assert_eq!(
            code.get_synthetize_parameters(),
            (Language::French, "Bette".to_string())
        );
        Ok(())
    }

    #[test]
    fn test_orally_said() -> std::result::Result<(), Error> {
        assert_eq!(orally_said(&CountryCodes::FR, "", None), "");
        assert_eq!(
            orally_said(&CountryCodes::FR, "ra2?", None),
            "RaoulAnatoleDeux"
        );
        assert_eq!(
            orally_said(&CountryCodes::FR, "RA2!", None),
            "RaoulAnatoleDeux"
        );
        assert_eq!(
            orally_said(&CountryCodes::FR, "Zoé", None),
            "ZoéOscarEugène"
        );
        assert_eq!(
            orally_said(&CountryCodes::BE, "Zoé", None),
            "ZéroOscarÉmilie"
        );
        assert_eq!(orally_said(&CountryCodes::EO, "Zoé", None), "ZuluOskarEko");
        assert_eq!(orally_said(&CountryCodes::FR, "CQD 41,46 N, 50,14 W.", None),
            "CélestineQuintalDésiré QuatreUnDécimaleQuatreSix NicolasDécimale CinqZéroDécimaleUnQuatre WilliamSTOP");
        assert_eq!(
            orally_said(&CountryCodes::FR, "Zoé", Some(true)),
            "Zoé Oscar Eugène "
        );
        Ok(())
    }
    #[test]
    fn test_orally_heard() -> std::result::Result<(), Error> {
        assert_eq!(orally_heard(&CountryCodes::FR, ""), "");
        assert_eq!(orally_heard(&CountryCodes::FR, "RaoulAnatoleDeux?"), "RA2");
        assert_eq!(orally_heard(&CountryCodes::FR, "RAOULANATOLEDEUX!"), "RA2");
        assert_eq!(orally_heard(&CountryCodes::FR, "ZOÉOSCAREUGÈNE"), "ZOE");
        assert_eq!(
            orally_heard(&CountryCodes::BE, "ZéRo oscar Émilie"),
            "Z O E"
        );
        assert_eq!(orally_heard(&CountryCodes::EO, "ZULUOSKAREKO"), "ZOE");
        assert_eq!(orally_heard(&CountryCodes::FR, "CélestineQuintalDésiré QuatreUnDécimaleQuatreSix NicolasDécimale CinqZéroDécimaleUnQuatre WilliamSTOP"),
            "CQD 41,46 N, 50,14 W.");
        Ok(())
    }
}
